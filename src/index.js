import 'bootstrap/dist/css/bootstrap.css';
import '@shopify/polaris/dist/styles.css';
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { AppProvider } from '@shopify/polaris';
import { ConnectedRouter } from 'connected-react-router/immutable';
// import { Provider as AppProviderBridge } from '@shopify/app-bridge-react';
// import createApp from '@shopify/app-bridge';
// import { Redirect, Toast } from '@shopify/app-bridge/actions';
import enTranslations from '@shopify/polaris/locales/en.json';
import configureStore, { history } from './stores';
import App from './App';
import reportWebVitals from './reportWebVitals';

const store = configureStore(/* provide initial state if any */);
// const urlString = window.location.href;
// const url = new URL(urlString);
// const shopDomain = url.searchParams.get('shop');

// const config = {
//   shopOrigin: shopDomain,
//   apiKey: process.env.REACT_APP_KEY,
//   forceRedirect: false
// };
// const app = createApp(config);
// // Redirect component
// const redirect = Redirect.create(app);
// const toast = Toast.create(app, {
//   message: 'Downgrade your plan successfully'
// });
// window.redirect = redirect;
// window.toast = toast;

ReactDOM.render(
  <React.StrictMode>
    <AppProvider
      i18n={enTranslations}
      // features={{ newDesignLanguage: true }}
    >
      {/* <AppProviderBridge config={config}> */}
      <Provider store={store}>
        <ConnectedRouter history={history}>
          <App />
        </ConnectedRouter>
      </Provider>
      {/* </AppProviderBridge> */}
    </AppProvider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
