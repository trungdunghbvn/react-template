import { all } from 'redux-saga/effects';
import AppSaga from './app-sagas';

export default function* rootSagas() {
  yield all([AppSaga()]);
}
