/* eslint-disable import/prefer-default-export */
/* eslint-disable func-names */
import axios from 'axios';

function getVal(str) {
  const v = window.location.search.match(new RegExp(`(?:[?&]${str}=)([^&]+)`));
  return v ? v[1] : undefined;
}

const urlHref = window.location.href;
const hmacUrl = getVal('hmac');
const protocolUrl = getVal('protocol');
const timestampUrl = getVal('timestamp');
const localeUrl = getVal('locale');
const sessionUrl = getVal('session');

export function* callApiServiceCommon(payload) {
  const url = payload.url;
  const method = payload.method;
  const params = payload.params;
  if (typeof params !== 'object') {
    return yield;
  }
  const Array = Object.keys(params);
  // POST
  if (method === 'POST') {
    const bodyFormData = new FormData();
    yield Array.forEach(element => {
      bodyFormData.append(element, params[element]);
    });
    bodyFormData.append('hmac', hmacUrl);
    bodyFormData.append('locale', localeUrl);
    bodyFormData.append('protocol', protocolUrl);
    bodyFormData.append('timestamp', timestampUrl);
    bodyFormData.append('session', sessionUrl);
    bodyFormData.append('url', urlHref);
    return yield axios({
      method,
      url,
      data: bodyFormData,
      config: { headers: { 'Content-Type': 'multipart/form-data' } }
    })
      .then(response => {
        return response.status === 200 ? response.data : '';
      })
      .catch(error => {
        console.log(error);
      });
  }
  // GET
  return yield axios
    .get(url, {
      params
    })
    .then(function (response) {
      return response.status === 200 ? response.data : [];
    })
    .catch(function () {
      return [];
    });
}
