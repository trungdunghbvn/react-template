import { takeEvery } from 'redux-saga/effects';
import * as types from '../constants/index';

// import * as Service from './service';

export function* connectToAPageSaga() {
  yield console.log('dev');
}

function* AppSaga() {
  yield takeEvery(types.CONNECT_TO_A_PAGE, connectToAPageSaga);
}

export default AppSaga;
