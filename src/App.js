import React from 'react';
import Routers from './routes/Main';

function App() {
  return (
    <>
      <Routers />
    </>
  );
}

export default App;
