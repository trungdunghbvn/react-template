/* eslint-disable no-alert */
/* eslint-disable no-undef */
import React from 'react';
import { Card, Page, Thumbnail } from '@shopify/polaris';

function Home() {
  return (
    <Page
      breadcrumbs={[{ content: 'Products', url: '/products' }]}
      title='3/4 inch Leather pet collar'
      subtitle='Perfect for any pet'
      thumbnail={
        <Thumbnail
          source='https://burst.shopifycdn.com/photos/black-leather-choker-necklace_373x@2x.jpg'
          alt='Black leather pet collar'
        />
      }
      primaryAction={{ content: 'Save', disabled: true }}
      secondaryActions={[
        {
          content: 'Duplicate',
          accessibilityLabel: 'Secondary action label',
          onAction: () => alert('Duplicate action')
        },
        {
          content: 'View on your store',
          onAction: () => alert('View on your store action')
        }
      ]}
      actionGroups={[
        {
          title: 'Promote',
          accessibilityLabel: 'Action group label',
          actions: [
            {
              content: 'Share on Facebook',
              accessibilityLabel: 'Individual action label',
              onAction: () => alert('Share on Facebook action')
            }
          ]
        }
      ]}
      separator
    >
      <Card>
        <Card title='Online store dashboard' sectioned>
          <p>View a summary of your online store’s performance.</p>
        </Card>
      </Card>
    </Page>
  );
}

export default Home;
