import { fromJS } from 'immutable';
import * as types from '../constants';

const defaultState = fromJS({
  appStatus: false
});

const appReducer = (state = defaultState, action) => {
  switch (action.type) {
    case types.CHANGE_PAGE_ACTIONS:
      return state.set('pageActive', action.page);
    default:
      return state;
  }
};

export default appReducer;
