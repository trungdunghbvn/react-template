import React, { Suspense, lazy } from 'react';
import { Switch, Route } from 'react-router-dom';
// import LoadingComponent from '../components/LoadingComponent';

// import PageCommon from '../components/router/page-children/PageCommon';

const Home = lazy(() => import('../components/Home.js'));

const Main = () => (
  <main>
    <Suspense fallback={<div>Loading...</div>}>
      <Switch>
        <Route exact path='/' component={Home} />
      </Switch>
    </Suspense>
  </main>
);
export default Main;
